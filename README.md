Web OPI安装部署SOP
========
## [简介](#INTRODUCTION)
## [系统架构](#ARCHITECTURE)
## [硬件需求](#HARDWARE_ENV)
## [网络需求](#NETWORK_ENV)
## [部署](#DEPLOY)
>[软件环境需求](#SOFT_ENV)
>
>[数据库](#DB)
>
>[Nginx服务器](#NGINX_SERVER)
>
>[MQ服务器](#MQ_SERVER)
>
>[前端应用](#FRONT_APP)
>
>[后端应用](#BACKEND_APP)
>
>[His应用](#HIS_APP)
>
>[项目配置文件](#APP_CONF)

## [相关软件安装文件](#SOFTLIST)
## [相关软件定制配置文件](#SOFT_CONF_U)

#<a name="INTRODUCTION">简介
>本文档主要用于户指导Web系统的正常安装部署、启动。
>
>并不涉及对具体软件的详细配置、优化策略的解释。

#<a name="ARCHITECTURE">系统架构
>系统主要分为前端、后端和his三部分
>>前端
>>>使用一个nginx提供用户访问(如图：Nginx-F)

>>>Nginx-F仅负责分发请求，实际任务由部署在一个或多个tomcat(TomcatF-*)上的前端项目完成，（每个tomcat）实例均运行着一个相同的前端应用。

>>>前端应用：TomcatF-*
>>>>大部分请求会直接发给后端的nginx（Nginx-B）处理。

>>>>少部分请求会直接查询数据库

>>>配置文件
>>>>`/opt/icim/conf/icim.properties`
>>>>>存储后端应用的访问地址

>>>>>配置主备两个，默认连接PrimaryNginxServer

>>>>`{tomcat_HOME}/conf/Catalina/localhost/WebMES-V1.xml`
>>>>>存储DB连接参数

>>>>>`{tomcat_HOME}/lib/目录下需要放入mysql-connector-java-5.1.7-bin.jar`

>>后端
>>>使用一个nginx提供用户访问(如图：Nginx-B)

>>>Nginx-B同样仅负责分发请求，实际任务由部署在一个或多个tomcat(TomcatB-*)上的后端项目完成，（每个tomcat）实例均运行着一个相同的前端应用。

>>>后端应用：TomcatB-*
>>>>和DB交互完成任务操作

>>>>将对数据库的操作以XML格式发送到MQ中。

>>>配置文件
>>>>/opt/icim/conf/triger.txt
>>>>>存储请求和实际任务处理服务的对应关系

>>>>/opt/icim/conf/jrep_1.txt
>>>>>存储对哪些表的修改需要发送消息至MQ

>>>>{tomcat_HOME}/conf/Catalina/localhost/WebPPT.xml
>>>>>存储DB连接参数

>>His处理
>>>由部署在一个tomcat（TomcatH）上的His应用完成

>>>His应用：TomcatH
>>>>从MQ中取出后端应用放入的消息。

>>>>解析取到的消息，存储到DB中。

>>>配置文件
>>>>`/opt/icim/conf/LoadHistory.properties`
>>>>>存储MQ Server的连接参数
>>>>>存储His DB的连接参数

>
>![ARCHITECTURE_IMG](img/ar-v3.png)

#<a name="HARDWARE_ENV">硬件需求
#<a name="NETWORK_ENV">网络需求
#<a name="DEPLOY">部署
##<a name="SOFT_ENV">软件环境需求
>
>注：如无特殊说明，本文档中涉及的全部软件(OS、GCC/G++除外)的安装文件均会随本文档一同发布，详见[相关软件安装文件](#SOFTLIST)
>
>OS
>>当前系统化基于Red Hat Enterprise Linux Server release 5.4（x86_64）进行部署
>>
    $ cat /etc/issue
	Red Hat Enterprise Linux Server release 5.4 (Tikanga)
	$ uname -a
	Linux opitest01 2.6.18-164.el5 #1 SMP Tue Aug 18 15:51:48 EDT 2009 x86_64 x86_64 x86_64 GNU/Linux
>>
>>安装后关闭系统防火墙
>>
	/* 查看防火墙状态 */
	service iptables status
	/* 关闭防火墙 */
	service iptables stop
	/* 设置防火墙为默认关闭 */
	chkconfig iptables off
>>
>>新建用户cimusr属于组cimusr，密码为cimusr
>>
	[root@localhost src]# groupadd cimusr
	[root@localhost src]# useradd -g cimusr cimusr
	[root@localhost src]# passwd cimusr
	Changing password for user cimusr.
	New UNIX password: 
	Retype new UNIX password: 
	passwd: all authentication tokens updated successfully.
	[root@localhost src]# su cimusr
	Password: 
	[testuser@localhost src]$
>>
>>安装时请选择安装[GCC/G++](#GCC/G++)，勿安装mysql（OS自带版本较低，若误安装卸载即可）
>>
>
>JAVA
>>项目运行所需JAVA环境：jre 1.6 64-Bit
>>
	$ java -version
    java version "1.6.0_33"
    Java(TM) SE Runtime Environment (build 1.6.0_33-b03)
    Java HotSpot(TM) 64-Bit Server VM (build 20.8-b03, mixed 	mode)
>>

>安装
>>卸载老版本
>>>查看安装的jdk以及其依赖的文件
>>>
	#  rpm -aq|grep gcj
    java-1.4.2-gcj-compat-1.4.2.0-40jpp.115
    java-1.4.2-gcj-compat-devel-1.4.2.0-40jpp.115
    java-1.4.2-gcj-compat-src-1.4.2.0-40jpp.115
    libgcj-4.1.2-46.el5
    libgcj-devel-4.1.2-46.el5
    libgcj-devel-4.1.2-46.el5
    libgcj-src-4.1.2-46.el5
    libgcj-4.1.2-46.el5
    java-1.4.2-gcj-compat-devel-1.4.2.0-40jpp.115
>>>
>>>卸载
>>>
	rpm -e --nodeps java-1.4.2-gcj-compat-src-1.4.2.0-40jpp.115
    rpm -e --nodeps java-1.4.2-gcj-compat-1.4.2.0-40jpp.115
    rpm -e --nodeps --allmatches java-1.4.2-gcj-compat-devel-1.4.2.0-40jpp.115
    //使用allmatches是因为有两个java-1.4.2-gcj-compat-devel-1.4.2.0-40jpp.115，不用的话会出 现specifies multiple packages错误。
>>>

>>安装1.6
>>>在/usr/目录下新建java文件夹，将安装文件jdk-6u33-linux-x64.bin拷贝到/usr/java/下

>>>为jdk-6u33-linux-x64.bin添加权限：chmod +x jdk-6u33-linux-x64.bin

>>>解压安装文件
>>>
	[root@test01 java]# ./jdk-6u33-linux-x64.bin
    [root@test01 java]# ll
    total 70424
    drwxr-xr-x 8 root root     4096 Nov 19 17:17 jdk1.6.0_33
    -rwxr-xr-x 1 root root 72029591 Nov 19 17:17 jdk-6u33-linux-x64.bin
>>>

>>>配置环境变量

>>>vi /etc/environment,插入以下内容：
>>>
	JAVA_HOME=/usr/java/jdk1.6.0_33
    PATH="/usr/java/jdk1.6.0_33/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"
    CLASSPATH=.:/usr/java/jdk1.6.0_33/lib
>>>
>>>应用环境变量  ". /etc/environment"

>>>确认安装成功:
>>>
	[root@test01 java]# java -version
    java version "1.6.0_33"
    Java(TM) SE Runtime Environment (build 1.6.0_33-b03)
    Java HotSpot(TM) 64-Bit Server VM (build 20.8-b03, mixed mode)
>>>
>

><a name="GCC/G++">GCC/G++
>>应用软件需要源码编译安装，故需先安装GCC/G++
>>
	$ gcc --version
    gcc (Ubuntu 4.4.3-4ubuntu5.1) 4.4.3
    Copyright (C) 2009 Free Software Foundation, Inc.
    This is free software; see the source for copying conditions.  There is NO
    warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    $ g++ --version
    g++ (Ubuntu 4.4.3-4ubuntu5.1) 4.4.3
    Copyright (C) 2009 Free Software Foundation, Inc.
    This is free software; see the source for copying conditions.  There is NO
    warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
>>
>>安装：建议在OS安装时选择安装
>>
>

##<a name="DB">数据库
>数据库采用mysql5.0
>
>安装方式：使用RPM包安装
>
	MySQL-server-community-5.0.96-1.rhel5.x86_64.rpm
	MySQL-client-community-5.0.96-1.rhel5.x86_64.rpm
>

>安装
>>使用rpm -ivh分别安装MySQL-server和MySQL-client
>>
>>
	安装server
	[root@localhost mysql]# rpm -ivh MySQL-server-community-5.0.96-1.rhel5.x86_64.rpm  
	Preparing...                ########################################### [100%]
	   1:MySQL-server-community ########################################### [100%]
	PLEASE REMEMBER TO SET A PASSWORD FOR THE MySQL root USER !
	To do so, start the server, then issue the following commands:
	/usr/bin/mysqladmin -u root password 'new-password'
	/usr/bin/mysqladmin -u root -h localhost.localdomain password 'new-password'
>>
	Alternatively you can run:
	/usr/bin/mysql_secure_installation
>>	
	which will also give you the option of removing the test
	databases and anonymous user created by default.  This is
	strongly recommended for production servers.
>>	
	See the manual for more instructions.
>>	
	Please report any problems with the /usr/bin/mysqlbug script!
>>	
	The latest information about MySQL is available on the web at
	http://www.mysql.com
	Support MySQL by buying support/licenses at http://shop.mysql.com
	Starting MySQL.[  OK  ]
	Giving mysqld 2 seconds to start
>>
	查看mysql进程已经运行
	[root@localhost mysql]# ps -ef|grep mysql
	root      4606     1  0 20:15 pts/1    00:00:00 /bin/sh /usr/bin/mysqld_safe --datadir=/var/lib/mysql --pid-file=/var/lib/mysql/localhost.localdomain.pid
	mysql     4630  4606  1 20:15 pts/1    00:00:00 /usr/sbin/mysqld --basedir=/ --datadir=/var/lib/mysql --user=mysql --pid-file=/var/lib/mysql/localhost.localdomain.pid --skip-external-locking
	root      4667  4053  0 20:15 pts/1    00:00:00 grep mysql
>>	
	安装client
	[root@localhost mysql]# rpm -ivh MySQL-client-community-5.0.96-1.rhel5.x86_64.rpm 
	Preparing...                ########################################### [100%]
	   1:MySQL-client-community ########################################### [100%]
>>
	查看安装结果
	[root@localhost mysql]# rpm -qa |grep -i mysql
	MySQL-client-community-5.0.96-1.rhel5
	MySQL-server-community-5.0.96-1.rhel5
>>